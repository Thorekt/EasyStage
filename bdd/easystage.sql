-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  lun. 08 oct. 2018 à 10:55
-- Version du serveur :  5.7.19
-- Version de PHP :  7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `easystage`
--

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

CREATE TABLE `entreprise` (
  `id` int(11) NOT NULL,
  `nom` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ville` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cp` int(11) NOT NULL,
  `activite` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `entreprise`
--

INSERT INTO `entreprise` (`id`, `nom`, `mail`, `tel`, `adresse`, `ville`, `cp`, `activite`, `active`) VALUES
(1, 'Ecole Maternelle Françoise Dolto', '', '01 64 64 04 20', '30 avenue Georges Pompidou', 'Melun', 77000, 'Enseignement', 1),
(2, 'Groupe Adéquat', 'agence.vsd@groupeadequat.fr', '06 46 64 27 82', '390 avenue Anna Lindh', 'Vert-Saint-Denis', 77240, 'Autre', 1),
(3, 'Le Pain Gourmand', '', '07 50 60 12 06', '47 avenue Georges Pompidou', 'Melun', 77000, 'commerce/Distribution', 1),
(4, 'Franco Pizz', '', '01 64 37 55 07', '14 route de Montereau', 'Melun', 77000, 'Commerce/Distribution', 1),
(5, 'Papa Grill', '', '01 60 68 27 01', '26 Boulevard Gambetta', 'Melun', 77000, 'Hôtellerie/Restauration', 1),
(6, 'Ecole Primaire Jules Ferry', '', '01 64 38 99 78 ', 'Rue Jules Ferry ', 'Melun', 77000, 'Enseignement', 1),
(7, 'Tribunal de Grande Instance', '', '01 64 79 81 64', '2 avenue du Général Leclerc', 'Melun', 77010, 'Droit/Justice', 1),
(8, 'Baildy Group', '', '01 60 66 88 50', '1 place Loïc Baron', 'Melun', 77000, 'Multimedia/Audiovisuel/Informatique ', 1),
(9, 'Relay Gare', '', '01 64 39 33 21', 'Place Callieni', 'Melun', 77000, 'Commerce/Distribution', 1),
(10, 'Micro Crèche Les P’tis Petons', '', '09 82 42 27 12', '3 bis rue de l’église', 'St-Germain-Laxis', 77950, 'Sciences humaines ', 1),
(11, 'Diarra Institut', '', '01 64 09 81 17', 'Mail Gaillardon', 'Melun', 77000, 'Commerce/Distribution ', 1),
(12, 'Menuiserie', '', '01 60 88 25 59', '21 rue du Champ d’Epreuves', 'Corbeil Essonnes', 91100, 'Industrie', 1),
(13, 'Sodiaal International', '', '01 44 10 90 10', '170 bis Boulevard du Montparnasse', 'Paris Cedex 14', 75680, 'Industrie', 1),
(14, 'Intermarché', '', '01 60 56 00 90', '36 avenue du Général Patton', 'Melun', 77000, 'Commerce/Distribution', 1),
(15, 'Orange', '', '07 87 16 06 59', '187 avenue Daumesnil', 'Paris Cedex 14', 75012, 'Multimedia/Audiovisuel/Informatique ', 1),
(16, 'TMC télécommandes mécaniques par câble', '', '01 60 56 56 56', 'Rue Jean-Baptiste Colbert', 'Le Mée-Sur-Seine', 77350, 'Transports/Automobile ', 1),
(17, 'Sobeca', '', '01 64 52 04 30', '581 avenue de l’Europe', 'Vert-Saint-Denis', 77240, 'Bâtiment/Construction ', 1),
(18, 'Carrefour ', '', '01 69 64 64 00 ', '9 avenue du Lac Bois Briard Mermoz 6 Bat. K1', 'Evry', 91006, 'Commerce/Distribution', 1),
(19, 'Banque de France ', '', '01 64 87 67 03', '31 rue Croix des Petits Champs', 'Paris ', 75001, 'Banque/Assurance ', 1),
(20, 'Les Plaisirs Parisiens', '', '01 64 87 67 03', '40 Boulevard de l’Almont', 'Melun', 77000, 'Commerce/Distribution', 1),
(21, 'ND Logistics', '', '01 69 90 70 26', '9-11 rue des Haies Blanches', 'Le Coudray-Montceaux', 91830, 'Logistique ', 1),
(22, 'Banque de France ', '', '01 64 87 67 00 ', '24 rue St Ambroise', 'Melun', 77000, 'Banque/Assurance ', 1),
(23, 'Mairie de Melun', '', '01 64 52 33 03 ', '210 rue Paul Doumer', 'Melun', 77000, 'Fonction publique ', 1),
(24, 'CAMVS', '', ' 01 64 79 25 25 ', '297 rue Rousseau Vaudran', 'Dammarie-les-Lys', 77190, 'Fonction publique ', 1),
(25, 'La Banque Postale', '', '01 64 71 37 00 ', '210 avenue Georges Clémenceau', 'Melun', 77000, 'Banque/Assurance ', 1),
(26, 'SMR France', '', '01 64 79 22 00 ', '154 avenue du Lys', 'Dammarie-les-Lys', 77190, 'Transports/Automobile ', 1),
(27, 'PMS Photomike Studio', '', ' 01 64 64 04 23 ', 'Centre Commercial Carrefour Market', 'Rubelles', 77950, 'Multimedia/Audiovisuel/Informatique ', 1),
(28, 'Planet Pizza', '', '01 60 68 11 11 ', '80 bis rue du Général de Gaulle', 'Melun', 77000, 'Commerce/Distribution', 1),
(29, 'Ibis Styles Rubelles', '', '01 64 52 41 41 ', '6 rue du Perré', 'Rubelles', 77950, 'Hôtellerie/Restauration', 1),
(30, 'XPO SUPPLY CHAIN France', '', '01 48 16 38 00 ', 'Rue de Bourgogne – ZI de la Moinerie', 'Bretigny-sur-Orge', 91220, 'Transports/Automobile ', 1),
(31, 'Garage Jean Redele', '', '01 64 39 95 77 ', '17 rue du Général Leclerc', 'Brie-Comte-Robert', 77170, 'Transports/Automobile ', 1),
(32, 'Institut Cameane', '', '01 40 13 07 14 ', '23 rue des Jeuneurs', 'Paris ', 75002, 'Commerce/Distribution', 1),
(33, 'Général d’Optique', '', '01 60 63 01 23 ', 'Centre Commercial Boissénart', 'Cesson', 77240, 'Commerce/Distribution', 1),
(34, 'Euro Disney Associes ', '', '', '1 rue de la Galmy', 'Chessy', 77700, 'Art/Spectacle ', 1),
(35, 'Elres', '', '', '2 allée René Lalique', 'Combs-la-Ville', 77380, 'Commerce/Distribution', 1),
(36, 'Burton Of London', '', '01 64 09 24 82 ', '20 rue St Aspais', 'Melun', 77000, 'Commerce/Distribution', 1),
(37, 'Boulangerie Assia', '', '09 54 77 04 08 ', '12 rue Colonel Picot', 'Melun', 77000, 'Commerce/Distribution', 1),
(38, 'GEMO', '', ' 01 64 71 90 95 ', 'ZAC du Champs de Foire', 'Melun', 77000, 'Commerce/Distribution', 1),
(39, 'Association Lys ODE77', '', '01 64 39 97 93 ', '7 rue Marc Jacquet', 'Dammarie-les-Lys', 77190, 'Sciences humaines ', 1),
(40, 'Le Petrin de l’Abbaye', '', '', 'Centre Commercial de l’Abbaye', 'Dammarie-les-Lys', 77190, 'Commerce/Distribution', 1),
(41, 'Pharmacie Jaoui', '', '01 60 68 71 00 ', '65 avenue du Général Patton', 'Melun', 77000, 'Santé ', 1),
(42, 'Boulangerie l’Aures', '', '', '5-7 Square Blaise Pascal', 'Melun', 77000, 'Commerce/Distribution', 1),
(43, 'Centre DCNS de Nantes ', '', '02 40 84 89 49 ', 'Indret', 'La Montagne', 44620, 'Bâtiment/Construction ', 1),
(44, 'La Maison de Toutou', '', ' 01 64 37 15 61 ', '25 route de Montereau', 'Vaux-le-Penil', 77000, 'Santé ', 1),
(45, 'Direction des Affaires Sportives', '', '01 60 56 06 20 ', '4 rue de la Fontaine Saint Liesne', 'Melun', 77000, 'Fonction publique ', 1),
(46, 'SARL VHS', '', '', 'Centre Commercial Plein-Ciel', 'Le Mée-Sur-Seine', 77350, 'Commerce/Distribution', 1),
(47, 'Centre Hôspitalier de Melun', '', '01 64 71 60 00', '2 rue Fréteau de Peny', 'Melun', 77000, 'Santé ', 1),
(48, 'Hyper U', '', '01 64 05 23 01', 'Rue Gustave Eiffel', 'Brie-Comte-Robert', 77170, 'Commerce/Distribution', 1),
(49, 'Musée de la Gendarmerie', '', '01 64 14 54 64', 'Avenue du 13eme Dragon', 'Melun', 77000, 'Fonction publique ', 1),
(50, 'Boulangerie Pâtisserie Artisanale BAH', '', '', 'Avenue Maurice Dauvergne', 'Le Mée-Sur-Seine', 77350, 'Commerce/Distribution', 1),
(51, 'PAK Auto', '', '06 64 86 07 07 ', '112 route de Nangis', 'Vaux-le-Penil', 77000, 'Transports/Automobile ', 1),
(52, 'Boulanger', '', '0 825 85 08 50 ', 'Rue du Bois des Saint-Pères', 'Cesson', 77240, 'Commerce/Distribution', 1),
(53, 'Geolia', '', '09 72 13 20 41', '3 rue des Clotais', 'Champlan', 91160, 'Logistique ', 1),
(54, 'Cabinet de Kinésithérapie', '', '01 64 09 06 96', '17 Boulevard François René de Châteaubriand', 'Melun', 77000, 'Santé ', 1),
(55, 'Optiko', '', '01 60 66 01 80 ', '21 Boulevard de l’Almont', 'Melun', 77000, 'Commerce/Distribution', 1),
(56, 'A la Crêperie de l’Avenue', '', '01 64 52 59 25 ', '18 avenue du Général Patton', 'Melun', 77000, 'Hôtellerie/Restauration', 1),
(57, 'Avancial', '', '01 44 74 95 77 ', '40 avenue des Terroirs de France', 'Paris ', 75012, 'Transports/Automobile ', 1),
(58, 'Rigolo comme la Vie', '', '01 64 37 61 94 ', '5 rue Aimé Césaire', 'Cesson', 77240, 'Sciences humaines ', 1),
(59, 'Mairie de Villeneuve-Saint-Georges', '', '01 43 86 38 00 ', 'Place Pierre Semard', 'Villeneuve-Saint-Georges', 94190, 'Fonction publique ', 1),
(60, 'D.Lys Burger', '', '09 83 48 64 88 ', '824 avenue du Lys', 'Dammarie-les-Lys', 77190, 'Hôtellerie/Restauration', 1),
(61, 'Leroy Merlin', '', '01 69 53 59 59 ', '2 rue Aulnay Dracourt', 'Massy', 91300, 'Commerce/Distribution', 1),
(62, 'Okaïdi', '', '01 64 14 92 89 ', '28 rue Saint Aspais', 'Melun', 77000, 'Commerce/Distribution', 1),
(63, 'Auchan', '', '01 64 10 16 16 ', 'Centre Commercial Boissénart', 'Cesson', 77240, 'Commerce/Distribution', 1),
(64, 'O’Dwich', '', '01 75 18 29 02 ', '1 avenue du Général Patton', 'Melun', 77000, 'Hôtellerie/Restauration', 1),
(65, 'Kam Fret Services', '', '', '261 rue de Chanteloup', 'Le Mée-Sur-Seine', 77350, 'Transports/Automobile ', 1),
(66, 'Le Moulin à Sakina', '', '', '1 Avenue Patton', 'Melun', 77000, 'Commerce/Distribution', 1),
(67, 'Planète Santé Pharmacie', '', '01 60 56 54 44 ', '1 Square Blaise Pascal', 'Melun', 77000, 'Santé ', 1),
(68, 'Mairie de St-Germain-Laxis', '', '01 64 52 27 12 ', 'Place Emile Piot', 'St-Germain-Laxis', 77950, 'Fonction publique ', 1),
(69, 'Sun Paradise', '', '01 64 09 97 62 ', '13 rue de l’Abreuvoir', 'Melun', 77000, 'Commerce/Distribution ', 1),
(70, 'Optique Plein Ciel', '', '01 64 52 29 36 ', 'Centre Commercial Plein-Ciel', 'Le Mée-Sur-Seine', 77350, 'Commerce/Distribution', 1),
(71, 'ID Logistics France', '', '', '10 Rue Paul-Henri Spaak', 'Vert-Saint-Denis', 77240, 'Logistique ', 1),
(72, 'Mohammad Emran', '', '09 50 14 91 13', '49 rue de Bouville', 'Le Mée-Sur-Seine', 77350, 'Commerce/Distribution', 1),
(73, 'Mairie de Savigny-le-Temple', '', '01 64 10 18 00 ', '1 Place François Mitterrand', 'Savigny-le-Temple', 77176, 'Fonction publique ', 1),
(74, 'Tati', '', '01 60 66 83 83 ', 'ZAC du Champs de Foire', 'Melun', 77000, 'Commerce/Distribution', 1),
(75, 'Matrechka', '', '06 89 34 08 78 ', '18 rue Saint Barthélémy', 'Melun', 77000, 'Commerce/Distribution', 1),
(76, 'Multi Accueil AFC Les Petits Bergers', '', '01 64 39 91 61 ', 'Place de la Motte aux Cailles', 'Melun', 77000, 'Sciences humaines ', 1),
(77, 'Véronic Coiffure', '', '01 64 10 96 71 ', '25 route de Montereau', 'Vaux-le-Penil', 77000, 'Commerce/Distribution', 1),
(78, 'BNP Paribas', '', '0 820 82 00 01 ', '1 rue St Etienne', 'Melun', 77000, 'Banque/Assurance ', 1),
(79, 'Préfecture de Seine-et-Marne', '', '01 64 71 77 77 ', 'Rue des Saints-Pères', 'Melun', 77000, 'Fonction publique ', 1),
(80, 'Paulstra', '', ' 01 69 91 50 00 ', '24 rue de l’Eglantier', 'Lisses', 91090, 'Sport', 1),
(81, 'Vêt Affaires', '', '01 75 79 12 67 ', '137 rue de l’Industrie', 'Savigny-le-Temple', 77176, 'Commerce/Distribution', 1),
(82, 'Confédération Syndicale des Familles ', '', '01 64 38 51 63 ', '11 Avenue de Saint Exupery', 'Melun', 77000, 'Sciences humaines ', 1),
(83, 'Cornet Franck', '', '', '23 rue Georges Sand', 'Broyes', 51120, 'Bâtiment/Construction ', 1),
(84, 'Médiathèque', '', '01 60 56 04 70 ', '25 rue du Château', 'Melun', 77000, 'Fonction publique ', 1),
(85, 'EZO', '', '', 'Place Paul Gauguin', 'Dammarie-les-Lys', 77190, 'Industrie', 1),
(86, 'Mayeur Garage AD', '', ' 01 60 60 63 26 ', '24 avenue Jean Jaurès', 'Moissy-Cramayel', 77550, 'Transports/Automobile ', 1),
(87, 'Catchoupiote', '', '01 64 09 48 82 ', '11 rue du Presbytère ', 'Melun', 77000, 'Commerce/Distribution', 1),
(88, 'Bertin & Bertin Avocats Associés', '', '01 60 96 89 61', '2 rue Gambetta', 'Avon', 77210, 'Droit/Justice', 1),
(89, 'Boucherie de l’Almont', '', '01 64 09 20 08 ', '30 Boulevard de l’Almont', 'Melun', 77000, 'Commerce/Distribution', 1),
(90, 'Ecole Maternelle Montaigu', '', '01 64 52 33 03', '30 avenue Georges Pompidou', 'Melun', 77000, 'Enseignement', 1),
(91, 'Espace 3000 Vesoul', '', '03 84 97 10 10', 'Rue Victor Dolle', 'Vesoul', 70000, 'Transports/Automobile ', 1),
(92, 'Pierre et Beauté', '', '09 53 70 44 42', '21 rue de St Liesne', 'Melun', 77000, 'Commerce/Distribution', 1),
(93, 'RapidFlore', '', '01 60 66 47 01', '2 avenue du Général Patton', 'Melun', 77000, 'Commerce/Distribution', 1),
(94, 'Lidl', '', '01 60 56 00 90', '536 rue des Frères Thibault', 'Dammarie-les-Lys', 77191, 'Commerce/Distribution', 1),
(95, 'Librairie Jacques Amyot', '', '01 64 14 44 24', '22 rue Paul Doumer', 'Melun', 77000, 'Commerce/Distribution', 1),
(96, 'O.N. BTP', '', '06 61 07 95 24', '26 rue de la Fontaine', 'Melun', 77000, 'Bâtiment/Construction ', 1),
(97, 'Lycée Léonard de Vinci', '', '01 60 56 60 60 ', '2 bis rue Edouard Branly', 'Melun', 77011, 'Enseignement', 1),
(98, 'Picard Surgelés', '', '01 60 63 14 14', '8 rue de l’Orée du Bois', 'Savigny-le-Temple', 77176, 'Commerce/Distribution', 1),
(99, 'Centre Commercial', '', '01 48 40 17 90', '9 rue du Pré St Gervais', 'Pantin', 93500, 'Commerce/Distribution', 1),
(100, '3H Food', '', '09 81 47 52 37', 'Place des 3 Horloges', 'Melun', 77000, 'Hôtellerie/Restauration', 1),
(101, 'Pompes Funèbres Roc-Eclerc', '', '01 64 37 21 89', '603 avenue André Ampère', 'Dammarie-les-Lys', 77190, 'Commerce/Distribution', 1),
(102, 'Publicité Benoist', '', '01 64 37 15 75', '880 rue du Maréchal Juin', 'Vaux-le-Penil', 77300, 'Multimedia/Audiovisuel/Informatique ', 1),
(103, 'Maternité', '', '01 64 71 65 35', '2 rue de Fréteau de Peny', 'Melun', 77011, 'Santé ', 1),
(104, 'Clinique des Fontaines', '', '01 60 56 40 00 ', '54 Boulevard Aristide Briand', 'Melun', 77000, 'Santé ', 1),
(105, 'Maison Médicale Saint Nicolas', '', '01 60 66 84 45', 'Rue St Nicolas', 'Rubelles', 77950, 'Santé ', 1),
(106, 'Pharmacie', '', '01 64 37 36 07', 'Centre Commercial Villaubois', 'Dammarie-les-Lys', 77190, 'Santé ', 1),
(107, 'Auto Ecole', '', '09 83 34 06 77', '16 rue du Colonel Picot', 'Melun', 77000, 'Transports/Automobile ', 1),
(108, 'Hôtel Montaigne', '', '01 80 97 40 00', '6 avenue Montaigne', 'Paris ', 75008, 'Hôtellerie/Restauration', 1),
(109, 'Salon Fat Beauty', '', '06 12 78 01 25', '12 bis rue St Liesne', 'Melun', 77000, 'Commerce/Distribution', 1),
(110, 'Tonton Grill', '', '09 83 39 65 39', '49 rue St Barthélémy', 'Melun', 77000, 'Hôtellerie/Restauration', 1),
(111, 'Eva & Flo Couture', '', '01 64 39 50 56', '19 rue St Ambroise', 'Melun', 77000, 'Commerce/Distribution', 1),
(112, 'Clinique St Jean', '', '01 64 14 30 27', '41 avenue de Corbeil', 'Melun', 77000, 'Santé ', 1),
(113, 'Zadis Electrique', '', '01 60 65 19 53', '37 rue Marc Lanvin', 'Dammarie-les-Lys', 77190, 'Bâtiment/Construction ', 1),
(114, 'Centre de loisirs Bois du Lys', '', '01 64 37 15 27', '380 chemin du Clocher', 'Dammarie-les-Lys', 77190, 'Sport', 1),
(115, 'Chantemur', '', '01 60 68 25 00', 'ZAC du Champs de Foire', 'Melun', 77000, 'Commerce/Distribution', 1);

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`) VALUES
('20181004130717'),
('20181008105246');

-- --------------------------------------------------------

--
-- Structure de la table `stage`
--

CREATE TABLE `stage` (
  `id` int(11) NOT NULL,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `tuteur`
--

CREATE TABLE `tuteur` (
  `id` int(11) NOT NULL,
  `nom` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `role` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenm` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `entreprise`
--
ALTER TABLE `entreprise`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `stage`
--
ALTER TABLE `stage`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tuteur`
--
ALTER TABLE `tuteur`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `entreprise`
--
ALTER TABLE `entreprise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT pour la table `stage`
--
ALTER TABLE `stage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `tuteur`
--
ALTER TABLE `tuteur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
