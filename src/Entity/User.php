<?php

    namespace App\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use http\Exception\InvalidArgumentException;
    use Symfony\Component\Security\Core\User\UserInterface;
    use Symfony\Component\Validator\Constraints as Assert;
    use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

    /**
     * @ORM\Table(name="user")
     * @UniqueEntity(fields="mail")
     * @ORM\Entity
     */
    class User implements UserInterface, \Serializable
    {
        /**
         * @ORM\Id()
         * @ORM\GeneratedValue()
         * @ORM\Column(type="integer")
         */
        private $id;

        /**
         * @var array
         * @ORM\Column(name="Role", type="array")
         */
        private $roles;

        /**
         * @ORM\Column(name="Login", type="string", length=190)
         */
        private $login;

        /**
         * @ORM\Column(name="Password", type="string", length=190)
         */
        private $password;

        /**
         * @ORM\Column(name="Nom", type="string", length=190)
         */
        private $nom;
        /**
         * @ORM\Column(name="Prenm",type="string", length=190)
         */
        private $prenom;

        /**
         * @ORM\Column(name="Mail", type="string", length=190)
         */
        private $mail;

        /**
         * @Assert\NotBlank()
         * @Assert\Length(max=250)
         */
        private $plainPassword;

        /**
         * @ORM\Column(name="is_active", type="boolean")
         */
        private $isActive;


        public function __construct()
        {
            $this->isActive = true;
            $this->roles = ['ROLE_USER'];
        }

        public function getId()
        {
            return $this->id;
        }



        public function getRoles()
        {
            return $this->roles;
        }




        function addRole($role)
        {
            $this->roles[] = $role;
        }


        public function isRole($roleWanted)
        {
            return in_array($roleWanted, $this->roles);
        }

        public function getSalt()
        {
            return null;
        }

        public function eraseCredentials()
        {
        }

        /** @see \Serializable::serialize() */
        public function serialize()
        {
            return serialize(array(
                $this->id,
                $this->login,
                $this->password,
                $this->nom,
                $this->prenom,
                $this->mail,
                $this->isActive,
            ));
        }

        /** @see \Serializable::unserialize() */
        public function unserialize($serialized)
        {
            list (
                $this->id,
                $this->login,
                $this->password,
                $this->nom,
                $this->prenom,
                $this->mail,
                $this->isActive,
                ) = unserialize($serialized);
        }

        public function setRoles(array $role)
        {
            if (!in_array('ROLE_USER', $role)){
                $role[] = 'ROLE_USER';
            }
            foreach ($role as $roles){
                if(substr($roles, 0, 5) !== 'ROLE_')
                {
                    throw new InvalidArgumentException("Chaque rôle doit commencer par 'ROLE_'");
                }
            }
            $this->role = $role;
            return $this;
        }

        public function getUsername()
        {
            return $this->mail;
        }

        public function getLogin()
        {
            return $this->login;
        }

        public function setLogin($login)
        {
            $this->login = $login;
        }

        public function getPassword()
        {
            return $this->password;
        }

        public function setPassword($password)
        {
            $this->password = $password;
        }

        public function getNom()
        {
            return $this->nom;
        }

        public function setNom($nom)
        {
            $this->nom = $nom;

        }

        public function getPrenom()
        {
            return $this->prenom;
        }

        public function setPrenom($prenom)
        {
            $this->prenom = $prenom;
        }

        public function getMail()
        {
            return $this->mail;
        }

        public function setMail(string $mail)
        {
            $this->mail = $mail;
        }

        function getPlainPassword()
        {
            return $this->plainPassword;
        }

        function setPlainPassword($plainPassword)
        {
            $this->plainPassword = $plainPassword;

        }

        public function getIsActive()
        {
            return $this->isActive;
        }

        public function setIsActive($isActive)
        {
            $this->isActive = $isActive;
        }

    }
