<?php

namespace App\Controller;

use App\Entity\Entreprise;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class EntrepriseController extends AbstractController
{
    /**
     * @Route("/entreprise", name="entreprise")
     */
    public function index()
    {
        return $this->redirectToRoute("entreprise_list");
    }


    /**
     * @Route("/entreprise/list", name="entreprise_list")
     */
    public function showAll()
    {
        $entreprises = $this->getDoctrine()
            ->getRepository(Entreprise::class)
            ->findAll();

        return $this->render('entreprise/list.html.twig', array(
            'entreprises' => $entreprises,
            'pagename' => 'Liste des entreprises',
        ));
    }

    /**
     * @Route("/entreprise/new", name="entreprise_new")
     */
    public function createEntreprise(Request $request)
    {

        $entreprise = new Entreprise();
        $entreprise->setNom('');
        $entreprise->setMail('');
        $entreprise->setTel('');
        $entreprise->setAdresse('');
        $entreprise->setVille('');
        $entreprise->setCp('');
        $entreprise->setActivite('');
        $entreprise->setActive(0);

        $form = $this->createFormBuilder($entreprise)
            ->add('nom', TextType::class)
            ->add('mail', EmailType::class)
            ->add('tel', TextType::class)
            ->add('adresse', TextType::class)
            ->add('ville', TextType::class)
            ->add('cp', TextType::class)
            ->add('activite', TextType::class)
            ->add('active', ChoiceType::class, array(
                'label' => 'Etat : ',
                'attr' => ['class' => ' custom-select ', 'style' => ' max-width: 10%'],
                'choices' => array('Actif' => 1, 'Inactif' => 0),))
            ->add('save', SubmitType::class, array('label' => 'Valider', 'attr' => ['class' => 'btn btn-success']))
            ->getForm();
        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));

            if ($form->isSubmitted() && $form->isValid()) {
                $item = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($item);
                $em->flush();
                return $this->redirectToRoute("entreprise_display", array('id' => $entreprise->getId()));
            }
            return $this->redirectToRoute("entreprise_display", array('id' => $entreprise->getId()));

        }
        return $this->render('entreprise/form.html.twig', array(
            'form' => $form->createView(),
            'pagename' => "Ajout d'entreprise",
        ));


    }

    /**
     * @Route("/entreprise/update/{id}", name="entreprise_update")
     */
    public function update($id, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entreprise = $entityManager->getRepository(Entreprise::class)->find($id);

        if (!$entreprise) {
            throw $this->createNotFoundException(
                'No entreprise found for id ' . $id
            );
        }

        $form = $this->createFormBuilder($entreprise)
            ->add('nom', TextType::class)
            ->add('mail', EmailType::class)
            ->add('tel', TextType::class)
            ->add('adresse', TextType::class)
            ->add('ville', TextType::class)
            ->add('cp', TextType::class)
            ->add('activite', TextType::class)
            ->add('active', ChoiceType::class, array(
                'label' => 'Etat : ',
                'attr' => ['class' => ' custom-select ', 'style' => ' max-width: 10%'],
                'choices' => array('Actif' => 1, 'Inactif' => 0),))
            ->add('save', SubmitType::class, array('label' => 'Valider', 'attr' => ['class' => 'btn btn-success']))
            ->getForm();
        $form->handleRequest($request);
        //  dump($entreprise);
        // dump($form);
        //   die();
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entreprise);
            $em->flush();
            return $this->redirectToRoute("entreprise_display", array('id' => $entreprise->getId()));

//                throw $this->createNotFoundException(
//                     'isvalid not work'                );
        }
        return $this->render('entreprise/form.html.twig', array(
            'form' => $form->createView(),
            'pagename' => "Modification d'entreprise",
        ));
    }

    /**
     * @Route("/entreprise/{id}", name="entreprise_display")
     */
    public function showOne(Entreprise $entreprise)
    {

        return $this->render('entreprise/display.html.twig', [
            'entreprise' => $entreprise,
            'pagename' => $entreprise->getNom(),
        ]);
    }
}
