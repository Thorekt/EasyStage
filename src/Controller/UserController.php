<?php

namespace App\Controller;

use App\Entity\Eleve;
use App\Entity\Prof;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class UserController extends AbstractController
{
    /**
     * @Route("/user", name="user")
     */
    public function index()
    {
        return $this->render('user/index.html.twig', [
            'controller_name' => 'UserController', "pagename" => "Authentification"
        ]);
    }


    /**
     * @Route("/inscription", name="inscription")
     */
    public function inscriptionAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {    //1) build the form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        //2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //3) Encode the password (you could also do this via doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            //$user->setRoles("ROLE_USER");
            //4) save the user
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user
            $this->addFlash('success', 'Votre compte à bien été enregistré');
            return $this->redirectToRoute('login');
        }
        return $this->render('user/form.html.twig', ['form' => $form->createView(),
            'mainNavRegistration' => true, "pagename" => "Inscription"]);

    }


    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {
        //get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        //last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        //
        $form = $this->get('form.factory')
            ->createNamedBuilder(null)
            ->add('_username', TextType::class, ['label' => 'Login'])
            ->add('_password', PasswordType::class,
                ['label' => 'Mot de passe'])
            ->add('ok', SubmitType::class,
                ['label' => 'Ok', 'attr' => ['class' => 'btn btn-success']])
            ->getForm();

        return $this->render('user/login.html.twig', [
            'mainNavLogin' => true,
            'form' => $form->createView(),
            'last username' => $lastUsername,
            'error' => $error,
            "pagename" => "Login"
        ]);

    }


    /**
     * @Route("/admin/user_list", name="user_list")
     */
    public function listeUsers()

    {
        $users = $this->getDoctrine()
            ->getRepository(User::class)
            ->findAll();

        return $this->render('user/list.html.twig', [
            'pagename' => 'Liste des utilisateurs',
            'users' => $users,
        ]);
    }

    /**
     * @Route("/admin/role_edit/{id}", name="role_edit")
     */
    public function userEdit($id, Request $request)
    {


        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->find($id);
//        if (!$user) {
//            throw $this->createNotFoundException(
//                'No user found for id ' . $id
//            );
//        }
//        $form = $this->createFormBuilder($user)
//            ->add('roles', CollectionType::class, array(
//                'entry_type' => ChoiceType::class,
//                'entry_options' => array(
//                    'choices' => array(
//                        'prof' => 'ROLE_PROF',
//                        'eleve' => 'ROLE_ELEVE',
//                        'admin' => 'ROLE_ADMIN',
//                    ),
//                ),
//            ))
//
//            ->add('submit', SubmitType::class, ['label'=>'Envoyer', 'attr'=>['class'=>'btn btn-danger']])
//            ->getForm();
//        if ($form->isSubmitted() && $form->isValid()) {
//            $entityManager = $this->getDoctrine()->getManager();
//            $entityManager->persist($user);
//            $entityManager->flush();
//        }


        return $this->render('user/setRoles.html.twig', [/*'form' => $form->createView(),*/
            'user' => $user,
            'mainNavRegistration' => true, "pagename" => "Modification des roles d'utilisateur"]);

    }

    /**
     * @Route("/become_admin", name="become_admin")
     */
    public function becomeAdmin()
    {
        $user = $this->getUser();
        $user->addRole('ROLE_ADMIN');
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();
        return $this->redirectToRoute("index");
    }

    /**
     * @Route("/admin/role_add/{id}/{role}", name="role_add")
     */
    public function addRole($id, $role)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->find($id);
        if ($user->isRole($role)) {
            return $this->redirectToRoute("role_edit", array('id' => $id));
        }

        if ($role == 'ROLE_PROF' && !($user->isRole('ROLE_ELEVE'))) {
            $prof = new Prof($user);
            $entityManager->persist($prof);
            $user->addRole($role);
        } elseif ($role == 'ROLE_ELEVE' && !($user->isRole('ROLE_PROF'))) {
            $eleve = new Eleve($user);
            $entityManager->persist($eleve);
            $user->addRole($role);
        } elseif ($role == 'ROLE_ADMIN') {
            $user->addRole($role);
        } else {
            return $this->redirectToRoute("role_edit", array('id' => $id));
        }

        $entityManager->persist($user);
        $entityManager->flush();
        return $this->redirectToRoute("role_edit", array('id' => $id));
    }
}
