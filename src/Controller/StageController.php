<?php

namespace App\Controller;

use App\Entity\Eleve;
use App\Entity\Entreprise;
use App\Entity\Prof;
use App\Entity\Stage;
use App\Entity\Tuteur;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class StageController extends AbstractController
{
    /**
     * @Route("/stage", name="stage")
     */
    public function index()
    {
        return $this->redirectToRoute("stage_list");

    }

    /**
     * @Route("/stage/list", name="stage_list")
     */
    public function showAll()
    {


        $stages = $this->getDoctrine()
            ->getRepository(Stage::class)
            ->findAll();

        return $this->render('stage/list.html.twig', array(
            'stages' => $stages,
            'pagename' => 'Liste des stages',
        ));
    }

    /**
     * @Route("/eleve/stage_new", name="stage_new")
     */
    public function stage_new(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $eleve = $em->getRepository(Eleve::class)->findOneBy(['user' => $user->getId()]);
        $item = new Stage();
        $item->setEleve($eleve);

        $profs = $em->getRepository(Prof::class)
            ->findAll();

        $profs = $this->objetsToArrayBynom($profs, 1);

        $entreprises = $em->getRepository(Entreprise::class)
            ->findAll();

        $entreprises = $this->objetsToArrayBynom($entreprises, 0);

        $tuteurs = $em->getRepository(Tuteur::class)
            ->findAll();

        $tuteurs = $this->objetsToArrayBynom($tuteurs, 0);


        $form = $this->createFormBuilder($item)
            ->add('tuteur', ChoiceType::class, array(
                'label' => 'Tuteur : ',
                'attr' => ['class' => ' custom-select ', 'style' => ' max-width: 10%'],
                'choices' => $tuteurs,))
            ->add('prof', ChoiceType::class, array(
                'label' => 'Prof : ',
                'attr' => ['class' => ' custom-select ', 'style' => ' max-width: 10%'],
                'choices' => $profs,))
            ->add('entreprise', ChoiceType::class, array(
                'label' => 'Entreprise : ',
                'attr' => ['class' => ' custom-select ', 'style' => ' max-width: 10%'],
                'choices' => $entreprises,))
            ->add('dateDebut', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ))
            ->add('dateFin', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ))
            ->add('save', SubmitType::class, array('label' => 'Validation', 'attr' => ['class' => 'btn btn-success']))
            ->getForm();

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($item);
            $em->flush();
            return $this->redirectToRoute("stage_list");
        }

        return $this->render('stage/form.html.twig', array(
            'form' => $form->createView(),
            'pagename' => 'Formulaire stage',
        ));
    }

    /**
     * @Route("/stage/tuteur_new", name="tuteur_new")
     */
    public function addTuteur(Request $request){
        $tuteur= new Tuteur();
        $tuteur->setNom('');
        $tuteur->setPrenom('');

        $form = $this->createFormBuilder($tuteur)
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Ajout tuteur', 'attr' => ['class' => 'btn btn-success']))
            ->getForm();

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $item = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($item);
            $em->flush();
            return $this->redirectToRoute("stage_new");
        }

        return $this->render('stage/tuteur.html.twig', array(
            'form' => $form->createView(),
            'pagename' => 'Ajout tuteur',
        ));
    }

    public function objetsToArrayBynom($objets, $type)
    {
        $em = $this->getDoctrine()->getManager();
        $array = array();
        if ($type == 0) {
            foreach ($objets as $objet) {
                $array[$objet->getNom()] = $objet;
            }
        } else {
            foreach ($objets as $objet) {
                $user = $em->getRepository(User::class)->find($objet->getUser());
                $array[$user->getNom()] = $objet;
            }
        }
        return $array;
    }

}